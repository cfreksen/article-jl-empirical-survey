\documentclass[a4paper,10pt,english]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{babel}
\usepackage{microtype}
\usepackage[numbers]{natbib}
\bibliographystyle{plainnat}
\usepackage{todonotes}
\usepackage{hyperref}
\usepackage{cleveref}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\eps}{\varepsilon}

\newcommand{\jld}[1]{``\textsf{#1}''}

\newtheorem{theorem}{Theorem}

\title{An Empirical Survey of Johnson--Lindenstrauss Transforms}
\author{Casper Benjamin Freksen \and Kasper Green Larsen}

\begin{document}

\maketitle

\section{Introduction}
\label{sec:introduction}

\begin{theorem}[Johnson--Lindenstrauss lemma]
  \label{thm:jl}
  For every $d \in \N, \eps \in (0, 1)$, and $X \subset \R^d$ with
  $|X| = N$, there exists a function $f \colon X \to \R^m$, where
  $m = \Theta(\eps^{-2} \log N)$ such that for every $x, y \in X$
  \begin{equation}
    \label{eq:jl}
    \big|\, \|f(x) - f(y)\|_2^2 - \|x - y\|_2^2 \,\big| \leq \eps \|x - y\|_2^2.
  \end{equation}
\end{theorem}


\begin{theorem}[Distributional Johnson--Lindenstrauss lemma]
  \label{thm:distjl}
  For every $d \in \N$ and $\eps, \delta \in (0, 1)$, there exists a
  probability distribution $\mathcal{F}$ over linear functions
  $f \colon \R^d \to \R^m$, where $m = \Theta(\eps^{-2}\log\frac{1}{\delta})$
  such that for every $x \in \R^d$
  \begin{equation}
    \label{eq:distjl}
    \Pr_{f \sim \mathcal{F}}\Big[ \, \big| \,\|f(x)\|_2^2 - \|x\|_2^2\, \big| \leq \eps \|x\|_2^2 \,\Big] \geq 1 - \delta .
  \end{equation}
\end{theorem}

We say that a probability distribution $\mathcal{F}$ over functions
that is a witness to \cref{thm:distjl} is a
JL-distribution\todo{JL-dist vs. JL--dist}.

In their original proof of \cref{thm:jl,thm:distjl}
\citep{Johnson:1984:EoLMiaHS}, \citeauthor{Johnson:1984:EoLMiaHS}
constructed the embedding by defining $f(x) = Ax$, where
$A \in \R^{m \times d}$ is the first $m$ rows of a random orthogonal
matrix. The orthogonality, however, is not neccessary as it has been
shown that by sampling the entries of $A$ i.i.d. from some common
distributions including Gaussian \citep{Indyk:1998:ANNTRtCoD} and
Rademacher\footnote{The Rademacher distribution is the uniform
  distribution on $\{-1, 1\}$.} \citep{Achlioptas:2003:DFRPJLwBC}
scaled appropriately, the distribution over matrices will be a
JL-distribution.\todo{Consider making a note that we will ignore
  scaling for simplicity in the descriptions of the various
  JL-distributions}

Applying any of these embeddings to a vector consists of computing a
dense matrix-vector product and therefore takes $\bigO(m d)$ time (or
more precisely $\bigO(m \|x\|_0)$, where $\|x\|_0$ is the number of
non-zero entries in $x$). Of these variants, sampling i.i.d. from a
Rademacher distribution is the easiest to implement, so we will use
this variant in our experiments and refer to it as \jld{Random
  Projection}.

Much research has gone into finding JL-distributions over more
efficiently computable functions, and the two main approaches has been
to using sparse matrices or using the fast Fourier transform (FFT) to
compute specific matrix-vector products more efficiently.


\subsection{Sparse JL}
\label{sec:intro:sparse-jl}

If $A$ has $s$ nonzero entries per column, then $f(x) = Ax$ can be
computed in $\bigO(s \|x\|_0)$ time.  In addition to showing that
sampling entries i.i.d. from a Rademacher distribution,
\citet{Achlioptas:2003:DFRPJLwBC} also showed sampling 0 with
probability $2/3$ and a scaled Rademacher otherwise also gives a
JL-distribution, but with $s \approx m/3$. However, further
development into Sparse JL moved away from sampling each entry
independently, and is instead usually described in terms of hashing.

Based on heuristics used in \citet{Weinberger:2009:FHfLSML},
\citet{Dasgupta:2010:aSJLT} were able to construct and analyse the
first sparse JL-distribution with $s = o(m)$ for some ranges of $\eps$
and $\delta$. Their construction, which we will refer to as
\jld{SparseJL DKS}, works by sampling $s$ hash functions
$h_k \colon [d] \to \{-1, 1\} \times [m]$ independently, such that
each source entry $x_i$ will be hashed to $s$ random signs
$\sigma_{ik}$ and $s$ target coordinates $j_{ik}$ (with
replacement). The function can then be defined as
$f(x) = \sum_{i} \sum_{k} e_{j_{ik}} \sigma_{ik} x_i$. Their own
analysis showed that this construction allows
$s = \bigO(\eps^{-1} \log^2\frac{m}{\delta} \log \frac{1}{\delta})$,
and later analysis \citep{Kane:2010:aDSJLT} tightened this bound to
$s = \bigO(\eps^{-1} \log\frac{m}{\delta} \log \frac{1}{\delta})$.

\citet{Kane:2014:SJLT} showed that the sparsity cannot be improved for
\jld{SparseJL DKS}, but they presented two other constructions that do
allow better sparsity, namely
$s = \bigO(\eps^{-1} \log \frac{1}{\delta})$, by ensuring that there
are no hash collisions within each input coordinate. \jld{SparseJL
  graph} is the same as \jld{SparseJL DKS} except that the hash
functions are dependent as the target coordinates are chosen without
replacement for each $i$, i.e. $j_{ia} \neq j_{ib}$ when $a \neq
b$. \jld{SparseJL block} partitions the target vector into $s$ blocks
of length $m/s$, and each hash function hashes into its own block. The
sparsity bounds for \jld{SparseJL graph} and \jld{SparseJL block} is
tight \citep{Kane:2014:SJLT}, and in general it has been shown that
$s = \Omega(\eps^{-1} \log \frac{1}{\delta} / \log \eps^{-1})$ for any
SparseJL construction when $m = O(d / \log \eps^{-1})$
\citep{Nelson:2013:SLBfDRM}.

Despite the sparsity lower bound, even sparser constructions have
found use in practice\todo{Consider referencing FH use
  cases}. \jld{Feature Hashing} is a special case of the Sparse JL
constructions with $s = 1$ \citep{Weinberger:2009:FHfLSML}, and by
restricting the input to vectors with a low $\ell_\infty / \ell_2$
ratio one can get JL guarantees analogous to \cref{thm:distjl}
\citep{Freksen:2018:FUtHT}. \citet{Jagadeesan:2019:USJLfFH} has
generalised this to the case where
$1 \leq s \leq O(\eps^{-1} \log \frac{1}{\delta})$, by showing how the
input restrictions loosens as $s$ increases for \jld{SparseJL block}.


\subsection{FFT JL}
\label{sec:intro:fft-jl}

The first JL-distribution with embedding time $o(m d)$ even when
$\|x\|_0 = \Theta(d)$ was the fast Johnson--Lindenstrauss transform
\citep{Ailon:2009:tFJLTaANN}, which we will refer to as \jld{FJLT}. It
is defined as $f(x) = PHDx$, where $P$ is a sparse matrix entries
sampled i.i.d. such that
$s \approx O(d^{-1} \eps^{-2} \log^3 \frac{1}{\delta})$ and non-zero
entries are drawn from a Gaussian distribution, $H$ is the
$d \times d$ Walsh--Hadamard matrix, and $D$ is a diagonal
$d \times d$ matrix with i.i.d. Rademachers along the diagonal. For
all Walsh--Hadamard based algorithms we assume that $d$ is a power of
2, if it is not we pad $x$ with zeroes to the next power of 2. Since
applying random signs and the Hadamard transform can be done
efficiently the embedding time for \jld{FJLT} is
$O(d \log d + \eps^{-2} \log^3
\frac{1}{\delta})$. \citet{Matousek:2008:oVotJLL} simplified the
construction by showing that the non-zero entries in $P$ can be
sampled from a Rademacher distribution.

Furthermore, since the key argument in the analysis of \jld{FJLT} is
that the $\ell_\infty / \ell_2$ ratio of $HDx$ is low with high
probability allowing for $P$ to be very sparse, we choose to implement
$P$ as \jld{SparseJL block}, as that has good theoretical guarantees
in this regime \citep{Jagadeesan:2019:USJLfFH}.

The embedding time improvement of \jld{FJLT} over \jld{Random
  Projection} depends on the relationship between $m$ and $d$. If
$m = \bigO(d^{1/3})$ then the embedding time is $\bigO(d \log d)$, but
at $m = \Theta(d^{1/2})$ \jld{FJLT} embeds in time $\bigO(m d \eps^4)$
\todo{Consider being more strict about bigO vs Theta}, only barely
faster than \jld{Random Projection}.

\citet{Ailon:2009:FDRURSoDBC} improved the running time of \jld{FJLT}
to $\bigO(d \log m)$ for $m = \bigO(d^{1/2 - \gamma})$ for any fixed
$\gamma > 0$. The increased applicable range of $m$ was achieved by
applying $HD^{(i)}$ iteratively a constant number of times with
independent $D^{(i)}$ matrices as well replacing $P$ with $BD$ where
$D$ is yet another diagonal matrix with Rademacher entries and $B$ is
consecutive blocks of specific partial Walsh--Hadamard matrices
(so-called binary dual BCH codes\todo{Consider citing ECC
  literature}). The reduction in running time comes from altering
transform slightly by partitioning the input into consecutive blocks
of length $\text{poly}(m)$ and applying the Walsh--Hadamard transforms
to each of them independently.

Another branch of research on fast JL-distributions has been on
suboptimal $m$, i.e. $m = \omega(\eps^{-2}\log\frac{1}{\delta})$, but
fast embedding time $\bigO(d \log d)$, even when $m$ is close to
$d$. \citet{Hinrichs:2011:JLLfCM} proposed using random
Toeplitz\footnote{A Toeplitz matrix is a matrix where every entry on
  an left to right descending diagonal has the same value.} matrices
based on i.i.d. Rademachers. Letting $f(x) = TDx$, where $T$ is a
Toeplitz matrix and $D$ is a diagonal matrix both with
i.i.d. Rademacher entries, gives a JL-distribution except with
$m = \bigO(\eps^{-2} \log^3 \frac{1}{\delta})$, which we will refer to
as \jld{Toeplitz}. Later and tighter
analysis\citep{Vybiral:2011:aVotJLLfCM} improved the upper bound to
$m = \bigO(\eps^{-2} \log^2 \frac{1}{\delta})$, but a more recent
result\citep{Freksen:2020:oUTaCMfJLT} showed a matching lower bound
for small enough $\eps$. Since computing the matrix-vector product
with a Toeplitz matrix corresponds to computing (part of) a
convolution, this can be done efficiently using FFT, giving an overall
embedding time of $\bigO(d \log d)$, which can be improved to
$\bigO(d \log m)$ by partitioning the input into blocks of length $m$
and and computing the convolution on each of them independently.

Similarly fast JL-distributions with suboptimal $m$ have been created
based on matrices with the so-called restricted isometry property
(RIP). A matrix is $(k, \eps)$-RIP if it as a linear map preserves the
$\ell_2$ norm squared within a factor of $1 \pm \eps$ for all vectors
$x$ with $\|x\|_0 \leq k$\todo{Should this be more formal?}. Letting
$f(x) = RHDx$, where $R$ is a random $m \times d$ matrix sampling $m$
coordinates with replacement, i.e. each row has one non-zero entry
with value 1 chosen uniformely at random, and $H$ and $D$ are a
Walsh--Hadamard matrix and a diagonal matrix with Rademacher entries,
\citet{Ailon:2013:aAOUFJLT} showed that this gives a JL-distribution
for $m = \bigO(\eps^{-4} \log \frac{1}{\delta} \log^4 d)$. We will
refer to this construction as \jld{RHD}. A key point in the analysis
of \jld{RHD} is that $RH$ is
$(\eps^{-2} \log \frac{1}{\delta}, \eps)$-RIP with high
probability. \citet{Krahmer:2011:NaIJLEvtRIP} generalised and improved
this result, showing how to derive a JL-distribution from any RIP
matrix, and for the $RH$ matrix they reduced the sufficient target
dimension to $m = \bigO(\eps^{-2} \log \frac{1}{\delta} \log^4 d)$. It
should be noted that a Toeplitz matrix based on Rademachers is also
RIP with high probability, which together with
\citet{Krahmer:2011:NaIJLEvtRIP} gives an upper bound on the
sufficient target dimension for \jld{Toeplitz} of
$m = \bigO\big(\max(\eps^{-1}\log^{3/2} \frac{1}{\delta} \log^{3/2} d,
\eps^{-2} \log \frac{1}{\delta} \log^4 d) \big)$.

Finally, we will describe two JL-distributions that use the idea of
quickly embedding into a suboptimal ``middle'' dimension $n$, and then
more slowly (but w.r.t. $n$) embed into the optimal target
dimension. The first such distribution was proposed by
\citet{Bamberger:2017:OFJLEfLDS} and is a combination of \jld{Random
  Projection} and \jld{RHD}. Let $f(x) = GRHDx$, where $G$ is a
\jld{Random Projection} matrix of size $m \times n$ with
$n = \Theta(\eps^{-2} \log \frac{1}{\delta} \log^4 d)$ and $RHD$ is as
defined in \jld{RHD}. This construction has an embedding time of
$\bigO(d \log m)$ for $m = \bigO(d^{1/2 - \gamma})$ for any fixed
$\gamma > 0$, similar to \citet{Ailon:2009:FDRURSoDBC}, but unlike
\citet{Ailon:2009:FDRURSoDBC} for any $r \in [1/2, 1]$ and
$m = \bigO(d^r)$, the embedding time becomes $\bigO(d^{2r} \log^4
d)$. However, the main strength of \jld{GRHD} is that it allows the
simultaneous embedding of $N$ points to be computed in total time
$\bigO(N d \log m)$, even when $m = \bigO(d^{1 - \gamma})$ for any
fixed $\gamma > 0$, by utilising fast matrix-matrix multiplication
techniques\citep{Lotti:1983:otACoRMM}.

The last JL-distribution we will mention is embeds quickly into the
suboptimal dimension using a so-called lean Walsh transform
(LWT)\citep{Liberty:2011:DFRPaLWT}. A $r \times c$ complex matrix
$A_1$ is a seed matrix if $r < c$, its columns are of unit length, and
its rows are pairwise orthogonal and have the same $\ell_2$ norm. As
such, partial Walsh--Hadamard matrices and partial Fourier matrices
are seed matrices (up to normalisation). We can then define a LWT of
order $l$ based on this seed as
$A_l = A_1^{\otimes l} = A_1 \otimes \dotsb \otimes A_1$, where
$\otimes$ denotes the Kronecker product. Note that $A_l$ is a
$r^l \times c^l$ matrix and that e.g. any Walsh--Hadamard transform
can be written as $A_l$ for some $l$ and the $2 \times 2$
Walsh--Hadamard matrix\footnote{Here we ignore the $r < c$ requirement
  of seed matrices.} as $A_1$. Furthermore, for a constant sized seed
the time complexity of applying $A_l$ to a vector is $O(c^l)$ by using
an algorithm similar to FFT. With LWT defined, given any $r \times c$
seed matrix we can define the JL-distribution as $f(x) = RA_lDx$,
where $R$ is a \jld{FJLT} from $r^l$ to $m$, $A_l$ is a LWT, and $D$
is a diagonal matrix with Rademacher entries. We will refer to this
JL-distribution as \jld{LWT}. Note that we assume that $d = c^l$,
otherwise we can just pad $x$ with zeroes to the next power of
$c$. Since applying LWT takes $\bigO(d)$ time and \jld{FJLT} is
applied on a polynomially smaller input, the total embedding time is
$\bigO(d)$. The caveat is that similarly to \jld{Feature Hashing}, one
needs to restrict the $\ell_\infty / \ell_2$ ratio of the input points
to get guarantees analogous to \cref{thm:distjl}.


\subsection{Our contributions}
\label{sec:our-contributions}

In order to measure embedding time and embedding quality of synthetic
and real world data we implemented various
JL-distributions\citep{Freksen:2020:code}, namely: \jld{Random
  Projection}, \jld{SparseJL DKS}, \jld{SparseJL block}, \jld{Feature
  Hashing}, \jld{FJLT}, \jld{Toeplitz}, \jld{GRHD}, and
\jld{LWT}\footnote{We did not include \citet{Ailon:2009:FDRURSoDBC}
  due to difficulties implementing the $B$ coding matrix.}. This
allowed us to compare how they performed while varying the following
parameters\todo{Are there more things to add here?}
\begin{enumerate}
\item Source and target dimension ($d$ and
  $m$), \label{item:param:dims}
\item The type of input data, especially theoretically hard
  instances, \label{item:param:input}
\item Hash function implementations for
  SparseJL, \label{item:param:hash}
\item Precomputing hash values for
  SparseJL, \label{item:param:eagerhash}
\item Middle dimension $n$ for \jld{GRHD},
  and \label{item:param:grhdmiddle}
\item Seed matrix sizes for \jld{LWT}. \label{item:param:lwtmiddle}
\end{enumerate}

\todo{Summarize the findings}

In order to measure embedding time accurately the JL-distributions
have been implemented in C++ and built on top of the efficient linear
algebra libraries: FFTW\citep{Frigo:2005:tDaIoFFTW},
GSL\citep{Galassi:2009:GSLRM}, OpenBLAS\citep{xianyi:2020:openblas},
and FFHT\citep{Andoni:2015:PaOLSHfAD}.

\subsection{Previous work}
\label{sec:previous-work}

\citet{Venkatasubramanian:2011:tJLTaES} is a similar empirical survey
of JL-distributions with experiments for both embedding time and
embedding quality. However, their survey only covered a subset of the
JL-distributions in our paper and only varied parameter
\cref{item:param:dims,item:param:lwtmiddle} when comparing those
JL-distributions.

\citet{Fedoruk:2018:DRvtJLLTaEBoED} is another Johnson--Lindenstrauss
paper with an empirical analysis of the
embedding. \citeauthor{Fedoruk:2018:DRvtJLLTaEBoED} focused solely on
the embedding quality of JL-distributions in the style of
\citet{Achlioptas:2003:DFRPJLwBC}, and how they performed on various
synthetic and real world data. Their paper does, however, include some
visual comparisons to two other (non-JL) dimensionality reduction
approaches: Principal component analysis and locally linear embedding.

\section{Embedding time}
\label{sec:embed-time}


\section{Embedding quality}
\label{sec:embed-quality}


\subsection{Hard instances}
\label{sec:qual:hard-instances}


\subsection{Real data}
\label{sec:qual:real-data}



\bibliography{references}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
